import 'package:app_localization/base_provider.dart';
import 'package:app_localization/language_bloc.dart';
import 'package:app_localization/languages.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageSelectorPage extends StatelessWidget {
  static final List<String> languagesList = languages.supportedLanguages;
  static final List<String> languageCodesList = languages.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  @override
  Widget build(BuildContext context) {
    final LanguageBloc _bloc = Provider.of<LanguageBloc>(context);
    return Scaffold(
      body: ListView.builder(
        itemCount: languagesList.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () async {
              print(languagesList[index]);
//              If the already - selected language is not English
//              Then, change it to English
              if (languageCodesList[index] == 'ml') {
                // step one, save the chosen locale
                var prefs = await SharedPreferences.getInstance();
                await prefs.setString('language_code', 'ml');
                await prefs.setString('country_code', 'IN');
                print("saved malayalam");
              }
//              If the already - selected language is not English
//              Then, change it to English
              if (languageCodesList[index] == 'en') {
                // step one, save the chosen locale
                var prefs = await SharedPreferences.getInstance();
                await prefs.setString('language_code', 'en');
                await prefs.setString('country_code', 'IN');
                print("saved english");
              }
              _bloc.changeLocale(Locale(languageCodesList[index]));
              Navigator.pop(context);
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Text(
                  languagesList[index],
                  style: TextStyle(fontSize: 24.0),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
