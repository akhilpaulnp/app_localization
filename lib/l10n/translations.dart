import 'dart:async';

import 'package:app_localization/languages.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'messages_all.dart';

class Translations {
  static Future<Translations> load(Locale locale) {
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new Translations();
    });
  }

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }

  String get title {
    return Intl.message('Language Test App', name: 'title', desc: 'The application title');
  }

  String get changeLanguage {
    return Intl.message('Change Language', name: 'changeLanguage');
  }

  String get dialogMessage {
    return Intl.message('Show Dialog', name: 'dialogMessage');
  }
}

class TranslationsDelegate extends LocalizationsDelegate<Translations> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return languages.supportedLanguagesCodes.contains(locale.languageCode);
  }

  @override
  Future<Translations> load(Locale locale) {
    return Translations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<Translations> old) {
    return false;
  }
}
