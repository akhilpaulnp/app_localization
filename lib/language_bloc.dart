import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageBloc {
  final _locale = BehaviorSubject<Locale>();

  Observable<Locale> get appLocale => _locale.stream;

//  Locale get getAppLocale => _locale.value;

  // Change data
  Function(Locale) get changeLocale => _locale.sink.add;

  LanguageBloc() {
    fetchLocaleFromPreference();
  }

  void dispose() async {
    await _locale?.drain();
    _locale?.close();
  }

  fetchLocaleFromPreference() async {
    var prefs = await SharedPreferences.getInstance();

    if (prefs.getString('language_code') != null) {
      changeLocale(Locale(prefs.getString('language_code')));
    }
  }
}
