import 'dart:ui';

typedef void LocaleChangeCallback(Locale locale);

class Languages {
  // List of supported languages
  final List<String> supportedLanguages = ['മലയാളം', 'english'];

  final List<String> supportedLanguagesCodes = [
    "ml",
    "en",
  ];

  // Returns the list of supported Locales
  Iterable<Locale> supportedLocales() => supportedLanguagesCodes.map<Locale>((lang) => new Locale(lang, ''));

  ///
  /// Internals
  ///
  static final Languages _languages = new Languages._internal();

  factory Languages() {
    return _languages;
  }

  Languages._internal();
}

Languages languages = new Languages();
